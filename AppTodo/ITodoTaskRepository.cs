﻿// ITodoTaskRepository.cs
using System.Collections.Generic;
using System.Threading.Tasks;

public interface ITodoTaskRepository
{
    Task<IEnumerable<TodoTask>> GetAllAsync();
    Task<TodoTask> GetByIdAsync(int id);
    Task CreateAsync(TodoTask todoTask);
    Task UpdateAsync(TodoTask todoTask);
    Task DeleteAsync(int id);
}
