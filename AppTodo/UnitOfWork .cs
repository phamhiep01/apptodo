﻿// UnitOfWork.cs
using System;
using System.Threading.Tasks;

public class UnitOfWork : IDisposable
{
    private readonly ApplicationDbContext _context;

    public UnitOfWork(ApplicationDbContext context)
    {
        _context = context;
        TodoTaskRepository = new TodoTaskRepository(_context);
    }

    public ITodoTaskRepository TodoTaskRepository { get; private set; }

    public async Task SaveAsync()
    {
        await _context.SaveChangesAsync();
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}
