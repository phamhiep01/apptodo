using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using TodoApi;
using TodoListApi;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
 static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
builder.Services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlServer("Data Source=DESKTOP-9FP7ODG;Database=ToDo;Trusted_Connection=True;TrustServerCertificate=True"));

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

app.Run();
