﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

public class TodoTask
{
    public int Id { get; set; }

    [Required]
    public string Description { get; set; }

    public DateTime DueDate { get; set; }

    public bool IsComplete { get; set; }

    public string UserId { get; set; }
    public User User { get; set; }
}

public class User : IdentityUser
{
    // Thêm các thuộc tính người dùng tùy chỉnh nếu cần
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public bool IsActive { get; set; }

    // Các mối quan hệ
    public ICollection<TodoTask> TodoTasks { get; set; }
}
public class RegisterRequest
{
    public string Username { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    // Thêm các thuộc tính khác nếu cần
}

public class LoginRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
    // Thêm các thuộc tính khác nếu cần
}
