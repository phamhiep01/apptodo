﻿// TodoTaskRepository.cs
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class TodoTaskRepository : ITodoTaskRepository
{
    private readonly ApplicationDbContext _context;

    public TodoTaskRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<TodoTask>> GetAllAsync()
    {
        return await _context.TodoTasks.ToListAsync();
    }

    public async Task<TodoTask> GetByIdAsync(int id)
    {
        return await _context.TodoTasks.FindAsync(id);
    }

    public async Task CreateAsync(TodoTask todoTask)
    {
        _context.TodoTasks.Add(todoTask);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateAsync(TodoTask todoTask)
    {
        _context.Entry(todoTask).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        var todoTask = await _context.TodoTasks.FindAsync(id);
        _context.TodoTasks.Remove(todoTask);
        await _context.SaveChangesAsync();
    }
}
