﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TodoApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController] 
    public class TodoTasksController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;

        public TodoTasksController(UnitOfWork unitOfWork, UserManager<User> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoTask>>> GetTodoTasks()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
            {
                return Unauthorized();
            }

            var tasks = await _unitOfWork.TodoTaskRepository.GetAllAsync();
            return Ok(tasks);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TodoTask>> GetTodoTask(int id)
        {
            var todoTask = await _unitOfWork.TodoTaskRepository.GetByIdAsync(id);
            if (todoTask == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
            {
                return Unauthorized();
            }

            if (User.IsInRole("admin") || todoTask.UserId == user.Id)
            {
                return Ok(todoTask);
            }
            else
            {
                return Forbid();
            }
        }

        [HttpPost]
        public async Task<ActionResult<TodoTask>> PostTodoTask(TodoTask todoTask)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
            {
                return Unauthorized();
            }

            todoTask.UserId = user.Id;

            await _unitOfWork.TodoTaskRepository.CreateAsync(todoTask);
            await _unitOfWork.SaveAsync();

            return CreatedAtAction("GetTodoTask", new { id = todoTask.Id }, todoTask);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoTask(int id, TodoTask todoTask)
        {
            if (id != todoTask.Id)
            {
                return BadRequest();
            }

            var existingTask = await _unitOfWork.TodoTaskRepository.GetByIdAsync(id);
            if (existingTask == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
            {
                return Unauthorized();
            }

            if (User.IsInRole("admin") || existingTask.UserId == user.Id)
            {
                existingTask.Description = todoTask.Description;
                existingTask.DueDate = todoTask.DueDate;
                existingTask.IsComplete = todoTask.IsComplete;

                await _unitOfWork.TodoTaskRepository.UpdateAsync(existingTask);
                await _unitOfWork.SaveAsync();

                return NoContent();
            }
            else
            {
                return Forbid();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoTask(int id)
        {
            var todoTask = await _unitOfWork.TodoTaskRepository.GetByIdAsync(id);
            if (todoTask == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (user == null)
            {
                return Unauthorized();
            }

            if (User.IsInRole("admin") || todoTask.UserId == user.Id)
            {
                await _unitOfWork.TodoTaskRepository.DeleteAsync(id);
                await _unitOfWork.SaveAsync();

                return NoContent();
            }
            else
            {
                return Forbid();
            }
        }
    }
}
